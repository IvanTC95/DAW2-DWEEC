// Creamos una función a la que le pasamos una cadena de texto
// Devuelve una cadena de texto que dice si la cadena que se le ha pasado está en mayúsculas, minúsculas o una mezcla
function mayusMinus(texto) {
    var contMayus = 0;
    var contMinus = 0;

    textoArray = texto.split("");

    for (caracterKey in textoArray) {
        if (textoArray[caracterKey] == textoArray[caracterKey].toUpperCase()) {
            contMayus++;
        } else {
            contMinus++;
        }
    }

    if (contMayus > 0 && contMinus > 0) {
        return "La cadena de texto tiene mayúsculas y minúculas mezcladas.";
    } else if (contMayus > 0) {
        return "La cadena de texto está en mayúsculas.";
    } else if (contMinus > 0) {
        return "La cadena de texto está en minúculas.";
    }
}

alert(mayusMinus("unacad"));