// Definimos un array con las letras disponibles para el DNI
var letrasDNI = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];

var DNIUsuario = prompt("Introduzca su DNI:");

if (DNIUsuario.length > 9) {
    alert("A su DNI le sobran caracteres.");
} else if (DNIUsuario.length < 9) {
    alert("Su DNI está incompleto.");
} else {
    var DNIUsuarioNumero = DNIUsuario.substring(0, 8);
    var DNIUsuarioLetra = DNIUsuario.substring(8);

    if (DNIUsuarioNumero < 0 || DNIUsuarioNumero > 99999999) {
        alert("El número de su DNI es incorrecto.");
    } else {
        var DNIUsuarioLetraCalculada = letrasDNI[(DNIUsuarioNumero%23)];

        if (DNIUsuarioLetra !== DNIUsuarioLetraCalculada) {
            alert("La letra de su DNI es incorrecta.");
        } else {
            alert("Todo está corecto. Gracias por enviar su DNI.");
        }
    }
}
