// Creamos un array con diferentes valores
var valores = [true, 5, false, "hola", "adios", 2];    

// Vemos cual cadena de texto es mayor
console.log(valores[3] > valores[4]);

// Usando los dos booleanos, sacamos un true
console.log(valores[0] || valores[2]);

// Usando los dos booleanos, sacamos un false
console.log(valores[0] && valores[2]);

// Mostramos el resultado de las 5 operaciones matemáticas con los dos números
console.log(valores[1] + " + " + valores[5] + " = " + (valores[1] + valores[5]));
console.log(valores[1] + " - " + valores[5] + " = " + (valores[1] - valores[5]));
console.log(valores[1] + " * " + valores[5] + " = " + (valores[1] * valores[5]));
console.log(valores[1] + " / " + valores[5] + " = " + (valores[1] / valores[5]));
console.log(valores[1] + " % " + valores[5] + " = " + (valores[1] % valores[5]));