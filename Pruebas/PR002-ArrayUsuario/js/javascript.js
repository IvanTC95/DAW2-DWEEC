var arrayUsuario = [];

// Mostramos el campo de texto al usuario mientras que introduzca elementos
do {
    elemento = prompt("Introduzca el elemento que desea introducir:\nSi no desea introducir más elementos, pulse enter.");
    arrayUsuario.push(elemento);
} while (elemento != "");

// Eliminamos el último elemento introducido (vacio)
arrayUsuario.pop();

// Mostramos el array del usuario por pantalla
for (elementoKey in arrayUsuario) {
    document.write(arrayUsuario[elementoKey]+"<br />");
}